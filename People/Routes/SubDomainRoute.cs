﻿using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace People.Routes
{
    public class SubaDomainRoute : RouteBase
    {
        public override RouteData GetRouteData(HttpContextBase httpContext)
        {
            string url = httpContext.Request.Headers["HOST"];
            string filePath = httpContext.Request.Path;
            var domainName = ConfigurationManager.AppSettings["DomanName"];
            int index = url.LastIndexOf(domainName);

            if (index <= 0)
            {
                return null;
            }
            string subDomain = url.Substring(0, index - 1);
            //if subdomain != some known values then route to th
            if (subDomain != "" && subDomain != "www" && subDomain != "mail")
            {
                var routeData = new RouteData(this, new MvcRouteHandler());
                routeData.Values.Add("controller", "Url");
 
//                routeData.Values.Add("action", "Index");
//                routeData.Values.Add("id", subDomain);

                string actionName = GetActionName(filePath);
                string idName = GetId(filePath);
                
                if (string.IsNullOrEmpty(actionName))
                {
                    routeData.Values.Add("action", "Index");
                    routeData.Values.Add("id", subDomain);

                    return routeData;
                }
                routeData.Values.Add("action", actionName);
                if (idName != null)
                {
                    routeData.Values.Add("id", idName);
                }    

                return routeData;
            }
            return null;
        }

        private string GetActionName(string filePath)
        {
            string[] parameters = filePath.Split('/');
            if (parameters.Count() > 2)
            {
                return parameters[2];
            }
            return null;
        }

        private string GetId(string filePath)
        {
            string[] parameters = filePath.Split('/');
            if (parameters.Count() > 3)
            {
                return parameters[3];
            }
            return null;
        }

        public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
        {
            return null;
        }
    }
}