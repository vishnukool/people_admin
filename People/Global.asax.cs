﻿using System.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;
using People.App_Start;
using People.Infrastructure;
using People.Installers;
using WebMatrix.WebData;

namespace People
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private WindsorContainer windsorContainer;
        private readonly string connectionString = ConfigurationManager.AppSettings["People"];
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            AuthConfig.RefisterAuth();
            WebSecurity.InitializeDatabaseConnection(connectionString, "System.Data.SqlClient", "UserProfile", "Id", "UserName", autoCreateTables: true);
            windsorContainer = IocContainer.Container;
            windsorContainer.Install(FromAssembly.This());
            var windsorControllerFactory = new WindsorControllerFactory(windsorContainer.Kernel);
            ControllerBuilder.Current.SetControllerFactory(windsorControllerFactory);
            ConfigureApi(GlobalConfiguration.Configuration);
        }
        void ConfigureApi(HttpConfiguration config)
        {
            config.DependencyResolver = new WindsorDependencyResolver(windsorContainer);
        }

    }
}