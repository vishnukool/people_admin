﻿using Microsoft.Web.WebPages.OAuth;

namespace People.App_Start
{
    public static class AuthConfig
    {
        public static void RefisterAuth()
        {
            OAuthWebSecurity.RegisterTwitterClient(
                consumerKey: "aRrVBLX8jBjdXv9qWlHsaw",
                consumerSecret: "HFdVwt2e5NS1HryZkYDfe8fqsRFLicDMCFXclIKgBA4");

            OAuthWebSecurity.RegisterFacebookClient(
                appId: "319369914832518",
                appSecret: "fb42f7306b5804d6556ede6a6c597001");

            OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}