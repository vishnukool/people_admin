﻿using System;
using System.Web.Mvc;
using DAL.FNhibernate;
using DAL.FNhibernate.Entities;
using DAL.FNhibernate.Repositories;
using NHibernate;
using People.App_Start;

namespace People.Attributes
{
    public class MyPostUserAuthorizationAttribute : AuthorizeAttribute
    {
        private readonly IPostRepository postRepository;
        
        //I'm Sorry 
        public MyPostUserAuthorizationAttribute()
        {
            postRepository = IocContainer.Container.Resolve<IPostRepository>();
        }

        public MyPostUserAuthorizationAttribute(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            string id = filterContext.Controller.ValueProvider.GetValue("id").AttemptedValue;
           //todo: some parse checks?

            using (ISession session = StaticSessionManager.OpenSession())
            {
            NHibernate.Context.CurrentSessionContext.Bind(session);
            session.BeginTransaction();

            Posts posts = postRepository.GetPost(int.Parse(id));

            session.Transaction.Commit();

            if (posts != null && posts.UserProfile.UserName == filterContext.HttpContext.User.Identity.Name)
            {
                return;   
            }
            filterContext.Result = new ViewResult{ViewName = "AccessDenied"};
            }

        }
    }
}