﻿using System.ComponentModel.DataAnnotations;

namespace People.Models
{
    public class CommentsModel
    {
        public int PostId { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter a comment")]
        [StringLength(1000, ErrorMessage = "Comment cannot be longer than 1000 characters.")]
        public string Body { get; set; }
        
    }
}