﻿using System;
using System.Web.Mvc;
using DAL.FNhibernate;
using NHibernate;

namespace People.Filters
{
    public class InjectSessionAttribute : ActionFilterAttribute
    {
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var session = StaticSessionManager.OpenSession();
            NHibernate.Context.CurrentSessionContext.Bind(session);
            session.BeginTransaction();
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var currentSession = NHibernate.Context.CurrentSessionContext.Unbind(StaticSessionManager.SessionFactory);
            if (filterContext.Exception != null && !filterContext.ExceptionHandled)
            {
                currentSession.Transaction.Rollback();
            }
            else
            {
                currentSession.Transaction.Commit();
                currentSession.Flush();
                currentSession.Close();
            }

//            if (currentSession != null)
//                try
//                {
//                    currentSession.Transaction.Commit();
//                }
//                catch (Exception)
//                {
//                    currentSession.Transaction.Rollback();
//                }
//                finally
//                {
//                    currentSession.Close();
//                }

            base.OnActionExecuted(filterContext);
        }
    }
}