﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace People.Helpers
{
    public static class MenuHelper
    {
        public static MvcHtmlString GetMenuItemFor(this HtmlHelper htmlHelper, string linkText,string action ,string contoller, string currentContoller)
        {
            TagBuilder tagBuilder = new TagBuilder("li");
            if (string.Equals(contoller,currentContoller,StringComparison.OrdinalIgnoreCase))
            {
                tagBuilder.AddCssClass("active");
            }
            tagBuilder.InnerHtml = htmlHelper.ActionLink(linkText, action, contoller).ToHtmlString();
            return new MvcHtmlString(tagBuilder.ToString());
        }

    }
}