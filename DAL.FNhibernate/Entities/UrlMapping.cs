﻿namespace DAL.FNhibernate.Entities
{
    public class UrlMapping
    {
        public virtual string Url { get; set; }
        public virtual Posts Posts { get; set; }
        public virtual int Hits { get; set; }
    }
}