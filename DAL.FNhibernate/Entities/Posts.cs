﻿using System;
using System.Collections.Generic;

namespace DAL.FNhibernate.Entities
{
    public class Posts
    {
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Url { get; set; }
        public virtual string Body { get; set; }
        public virtual DateTime Samay { get; set; }
        public virtual IList<Comments> Comments { get; set; }
        public virtual bool Commentable { get; set; }
        public virtual bool ShowHits { get; set; }
        public virtual UserProfile UserProfile { get; set; }

        public Posts()
        {
            Comments = new List<Comments>();
        }
    }

}
