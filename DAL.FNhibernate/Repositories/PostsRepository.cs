﻿using DAL.FNhibernate.Entities;
using NHibernate;
using NHibernate.Criterion;

namespace DAL.FNhibernate.Repositories
{
    public class PostsRepository : BaseRepository, IPostRepository 
    {
        public PostsRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public Posts GetPost(int id)
        {
                ICriteria criteria = GetSession().CreateCriteria<Posts>().Add(Restrictions.Eq("Id", id));
                criteria.SetFetchMode("Comments", FetchMode.Eager);
                return criteria.UniqueResult<Posts>();
        }

        public void Save(Posts posts)
        {
                GetSession().SaveOrUpdate(posts);
        }
    }

    public interface IPostRepository
    {
        Posts GetPost(int id);
        void Save(Posts posts);
    }
}