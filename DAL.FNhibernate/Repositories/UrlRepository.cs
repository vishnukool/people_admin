﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DAL.FNhibernate.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace DAL.FNhibernate.Repositories
{
    public class UrlRepository : BaseRepository, IUrlRepository
    {
        public UrlRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        int UrlsPerPage = int.Parse(ConfigurationManager.AppSettings["SearchResultsPerPage"]);

        public IList<UrlMapping> GetAll()
        {
            return GetSession().Query<UrlMapping>().ToList();
        }
        
        public IList<UrlMapping> GetUrls(string url, int pageNumber, out int count)
        {
            var queryOver = GetSession().QueryOver<UrlMapping>().Where(mapping => mapping.Url.IsLike(url + "%"));
            count = queryOver.RowCount();

            return queryOver
                .Skip(pageNumber)
                .Take(UrlsPerPage)
                .List<UrlMapping>();
        }

        public UrlMapping GetUrl(string url)
        {
                ICriteria criteria = GetSession().CreateCriteria<UrlMapping>().Add(Restrictions.Eq("Url", url));
                UrlMapping urlMapping = criteria.UniqueResult<UrlMapping>();
            return urlMapping;
        }

        public Posts GetPost(string url)
        {
                ICriteria criteria = GetSession().CreateCriteria<UrlMapping>().Add(Restrictions.Eq("Url", url));
                criteria.SetFetchMode("Posts", FetchMode.Eager);
                criteria.SetFetchMode("Posts.Comments", FetchMode.Eager);
                UrlMapping urlMapping = criteria.UniqueResult<UrlMapping>();
                return urlMapping == null ? null : urlMapping.Posts;
        }
        
        public UrlMapping GetUrlWithPost(string url)
        {
                ICriteria criteria = GetSession().CreateCriteria<UrlMapping>().Add(Restrictions.Eq("Url", url));
                criteria.SetFetchMode("Posts", FetchMode.Eager);
                criteria.SetFetchMode("Posts.Comments", FetchMode.Eager);
                UrlMapping urlMapping = criteria.UniqueResult<UrlMapping>();

                return urlMapping;
        }
        
        public UrlMapping GetUrlWithPost(Posts post)
        {

                ICriteria criteria = GetSession().CreateCriteria<UrlMapping>().Add(Restrictions.Eq("Posts", post));
                UrlMapping urlMapping = criteria.UniqueResult<UrlMapping>();
                return urlMapping;
        }

        public void Delete(UrlMapping urlWithPost)
        {
            GetSession().Delete(urlWithPost);
            GetSession().Delete(urlWithPost.Posts);
        }
        
        public void Save(UrlMapping urlWithPost)
        {
            GetSession().SaveOrUpdate(urlWithPost.Posts);
            GetSession().SaveOrUpdate(urlWithPost);
        }

        public string GetUrlForPost(Posts post)
        {

                ICriteria criteria = GetSession().CreateCriteria<UrlMapping>().Add(Restrictions.Eq("Posts", post));
                criteria.SetFetchMode("Posts", FetchMode.Lazy); //jst to be sure, i think lazy is default
                UrlMapping urlMapping = criteria.UniqueResult<UrlMapping>();
                return urlMapping.Url;
        }
       
    }

    public interface IUrlRepository
    {
        //todo: thanks to Mr. Vishnu's shitty design we ended up with 2 tables URl Mapping and Posts both having url :(
        // the UrlMapping domain has to go eventually go away 

        Posts GetPost(string url);
        UrlMapping GetUrlWithPost(string url);
        string GetUrlForPost(Posts post);
        UrlMapping GetUrlWithPost(Posts post);
        void Delete(UrlMapping urlWithPost);
        void Save(UrlMapping urlWithPost);
        IList<UrlMapping> GetAll();
        IList<UrlMapping> GetUrls(string urls, int pageNumber, out int count);
        UrlMapping GetUrl(string url);
    }
}