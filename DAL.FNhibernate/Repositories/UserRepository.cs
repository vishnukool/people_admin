﻿using DAL.FNhibernate.Entities;
using NHibernate;
using NHibernate.Criterion;

namespace DAL.FNhibernate.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public UserRepository(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public void CreateUser(string fullUserName, string friendlyName)
        {
            var session = GetSession();
            session.Save(new UserProfile
            {
                UserName = fullUserName,
                FriendlyName = friendlyName
            });
            session.Transaction.Commit();
            session.BeginTransaction();
        }

        public UserProfile GetUserWithPosts(string userName)
        {
            ICriteria criteria = GetSession().CreateCriteria<UserProfile>().Add(Restrictions.Eq("UserName", userName));
            criteria.SetFetchMode("Posts", FetchMode.Eager);
            var userWithPosts = criteria.UniqueResult<UserProfile>();
            return userWithPosts;
        }
        
        public UserProfile GetUser(string userName)
        {
            ICriteria criteria = GetSession().CreateCriteria<UserProfile>().Add(Restrictions.Eq("UserName", userName));
            var userWithPosts = criteria.UniqueResult<UserProfile>();
            return userWithPosts;
        }

    }
}
