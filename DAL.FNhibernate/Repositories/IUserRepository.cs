﻿using DAL.FNhibernate.Entities;

namespace DAL.FNhibernate.Repositories
{
    public interface IUserRepository
    {
       void CreateUser(string fullUserName, string friendlyName);
        UserProfile GetUserWithPosts(string userName);
        UserProfile GetUser(string userName);
    }
}
