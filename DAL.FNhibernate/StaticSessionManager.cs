﻿using System;
using System.Configuration;
using DAL.FNhibernate.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

namespace DAL.FNhibernate
{
    public class StaticSessionManager
    {
        public static readonly ISessionFactory SessionFactory;
        private static readonly string connectionString = ConfigurationManager.AppSettings["People"];

        static StaticSessionManager()
        {
            try
            {
                if (SessionFactory != null)
                    throw new Exception("trying to init SessionFactory twice!");

                SessionFactory = CreateSesionFactory();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                throw new Exception("NHibernate initialization failed", ex);
            }
        }
        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }


        public static ISessionFactory CreateSesionFactory()
        {
            FluentConfiguration fluentConfiguration = Fluently.Configure();
            FluentConfiguration configuration = fluentConfiguration
                .Database(MsSqlConfiguration
                              .MsSql2008
                              .ConnectionString(connectionString)
                              .ShowSql()
                )
                .ExposeConfiguration(configuration1 => configuration1.SetProperty("connection.release_mode", "on_close"))
                .ExposeConfiguration(configuration1 => new SchemaUpdate(configuration1).Execute(false, true))
                .ExposeConfiguration(configuration1 => configuration1.SetProperty("current_session_context_class", "web"));
            var sessionFactory = configuration
                .Mappings(m => m.AutoMappings.Add(AutoMap.AssemblyOf<Posts>()
                                                         .Where(type => type.Namespace.StartsWith("DAL.FNhibernate.Entities"))
                                                         .UseOverridesFromAssemblyOf<Posts>())).BuildSessionFactory();
            return sessionFactory;
        }


    }
}