﻿using DAL.FNhibernate.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DAL.FNhibernate.Maps
{
    public class PostsMappingOverride : IAutoMappingOverride<Posts>
    {
        public void Override(AutoMapping<Posts> mapping)
        {
            mapping.HasMany(posts => posts.Comments)
                .Inverse()
                   .Cascade.All();
            mapping.Map(posts => posts.Body).CustomType("StringClob").CustomSqlType("nvarchar(max)");
        }
    }
}
