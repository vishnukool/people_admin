﻿using DAL.FNhibernate.Entities;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace DAL.FNhibernate.Maps
{
    public class CommentsMappingOverride : IAutoMappingOverride<Comments>
    {
        public void Override(AutoMapping<Comments> mapping)
        {
            mapping.Map(posts => posts.Body).CustomType("StringClob").CustomSqlType("nvarchar(max)");
        }
    }
}